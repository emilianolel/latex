#!/bin/bash

clear

echo "-----------------------------------------------"
echo "--------------Plantillas de LaTeX--------------"
echo "-----------------------------------------------"

echo "°°°°°°°°°°°°Seleccione la plantilla°°°°°°°°°°°°"
echo "°°°°°°°Ingresa el número de la plantilla°°°°°°°"
echo "(0)	Todo Poderosa"
echo "(1)	Tareas"
echo "(2)	Ensayos"
echo "(3)	Poemas"
echo "(4)	Reportes"
read template

echo "----------Ingresa la ruta del archivo----------"
read archivo

touch $archivo

[ $template -eq 1 ] && cat ~/LaTeX/templates/template1.txt >> $archivo && vim $archivo
[ $template -eq 2 ] && cat ~/LaTeX/templates/template2.txt >> $archivo && vim $archivo
[ $template -eq 3 ] && cat ~/LaTeX/templates/template3.txt >> $archivo && vim $archivo
[ $template -eq 4 ] && cat ~/LaTeX/templates/template4.txt >> $archivo && vim $archivo
[ $template -eq 0 ] && cat ~/LaTeX/templates/todopoderosa.txt >> $archivo && vim $archivo

